from markdown.extensions import Extension
from markdown.inlinepatterns import SimpleTagPattern

DEL_RE = r'(--)(.*?)--'
INS_RE = r'(__)(.*?)__'
STRONG_RE = r'(\*\*)(.*?)\*\*'
EMPH_RE = r'(\/\/)(.*?)\/\/'

class TxtTagsExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        # Create the patterns
        del_tag = SimpleTagPattern(DEL_RE, 'del')
        ins_tag = SimpleTagPattern(INS_RE, 'ins')
        strong_tag = SimpleTagPattern(STRONG_RE, 'strong')
        emph_tag = SimpleTagPattern(EMPH_RE, 'em')
        # Insert patterns into markdown parser
        md.inlinePatterns.add('del', del_tag, '>not_strong')
        md.inlinePatterns.add('ins', ins_tag, '>del')
        md.inlinePatterns['strong'] = strong_tag
        md.inlinePatterns['emphasis'] = emph_tag
        del md.inlinePatterns['strong_em']
        del md.inlinePatterns['em_strong']
        del md.inlinePatterns['emphasis2']


## Thanks to https://blog.magicalhobo.com/2011/05/05/disabling-images-in-python-markdown/ 
class RemoveImageExtention(Extension):
    def extendMarkdown(self, md, md_globals):
        del md.inlinePatterns['image_link']
        del md.inlinePatterns['image_reference']
