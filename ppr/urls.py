"""ppr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from picc import views
from picc import urls as user_urls
from picc import dev_views

urlpatterns = [
    url(r'^captcha/', include('captcha.urls')),
    url(r'^$', views.index),
    url(r'^([0-9]{0,})$', views.index),
    url(r'^picc/(?P<pnum>[0-9]{1,})/$', views.picc, name='picc_view'),
    url(r'^suggest/new$', views.suggestNew),
    url(r'^suggest/vote$', views.suggestVote),
    url(r'^suggest/vote/done$', views.suggestVoteDone),
    url(r'^suggest/success$', views.suggestSuccess),
    url(r'^suggest/validate$', views.validatePiccs, name="validatePicc"),
    url(r'^picc/vote$', views.voteForPicc, name="votePicc"),
    url(r'^sendcomment$', views.sendComment, name="sendComment"),
    url(r'^login/', views.loginForm),
    url(r'^users/', include(user_urls)),
    url(r'^register/', views.register),
    url(r'^parseMD', views.parseMD),
    url(r'^random$', views.random),
    url(r'^test$', views.test),
    url(r'^admin/', admin.site.urls),
    url(r'^git/pull', dev_views.pull),
    ]
