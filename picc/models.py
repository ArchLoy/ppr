from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, Group

from . import utils
from . import md_utils
from . import mailer

from django.utils.encoding import python_2_unicode_compatible

from django.utils import timezone

@python_2_unicode_compatible
class Picc(models.Model):
    piccContent = models.TextField()
    userDisplayName = models.CharField(max_length=40)
    datePublish = models.DateField()
    commentCount = models.SmallIntegerField(default=0)
    favoriteCount = models.SmallIntegerField(default=0)
    voteUp = models.SmallIntegerField(default=0)
    voteDown = models.SmallIntegerField(default=0)
    idUsers = models.ForeignKey('Users', on_delete=models.DO_NOTHING, null=True, blank=True)
    votes = models.ManyToManyField('Users', related_name='userVotes')

    def __str__(self):
        return self.piccContent
    @staticmethod
    def create(s):
        Picc.objects.create(piccContent=s.content, datePublish=s.datesubmit, idUsers=s.idUsers, userDisplayName=s.idUsers.user.username)

@python_2_unicode_compatible
class Users(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    confirmCode = models.CharField(max_length=32, blank=True)
    avatar = models.FileField(upload_to=utils.user_directory_path, blank=True, default="/static/img/default.jpeg")
    piccVotesUp = models.SmallIntegerField(default=0)
    piccVotesDown = models.SmallIntegerField(default=0)
    commVoteUp = models.SmallIntegerField(default=0)
    commVoteDown = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.user.username

    @staticmethod
    def create_user(username, m_password, m_email):
        try:
            m_confirmCode = utils.textRnd()
            m_user = User.objects.create_user(username, password=m_password,
                     email=m_email, is_active=False)
            pUser = Users.objects.create(user=m_user, confirmCode=m_confirmCode, avatar='/static/img/default/default_avatar_'+utils.rndsi(25)+'.svg')
            pUser.groupAdd("base_user")
            pUser.save()
            mailer.send_confirmation_mail(pUser)
            return pUser
        except Exception as e:
            print(e)
            return None

    def groupAdd(self, m_group):
        try:
            g = Group.objects.get(name=m_group)
            self.user.groups.add(g)
        except Exception as e:
            print(e)

    # Get groups as string list, because of django developpers
    def getGroups(self):
        grplist = []
        for g in self.user.groups.all():
            grplist.append(g.name)

        return grplist

@python_2_unicode_compatible
class Comments(models.Model):
    comm = models.TextField()
    voteUp = models.SmallIntegerField(default=0)
    voteDown = models.SmallIntegerField(default=0)
    datePublish = models.DateTimeField(auto_now_add=True)
    idUsers = models.ForeignKey(Users, on_delete=models.DO_NOTHING, null=True, blank=True)
    idComments = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name="comments")
    picc = models.ForeignKey('Picc', on_delete=models.CASCADE)
    userDisplayName = models.CharField(max_length=40)

    def __str__(self):
        return self.comm

    def clean(self):
        self.comm = md_utils.processMd(self.comm)

@python_2_unicode_compatible
class Suggest(models.Model):
    content = models.TextField()
    datesubmit = models.DateField(auto_now_add=True)
    voteup = models.SmallIntegerField(default=0)
    votedown = models.SmallIntegerField(default=0)
    idUsers = models.ForeignKey('Users', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.content

    def clean(self):
        self.content = md_utils.processMd(self.content)

    def addVote(self, vote, user_):
        if vote == 1:
            self.voteup += 1
        else:
            self.votedown += 1

        self.suggest_vote_set.create(user=user_)
        self.save()

    def get_ratio(self):
        validatorsCnt = Group.objects.get(name='validator').user_set.count()
        votes = self.voteup + self.votedown
        ret = int((votes*100)/validatorsCnt)
        return ret

    def is_old(self):
        return self.datesubmit < utils.timesince(15).date()

    def is_voteup(self):
        if self.voteup > self.votedown:
            if (self.voteup*100)/(self.voteup+self.votedown) >= 60:
                return True

        return False

    def is_votedown(self):
        if self.votedown > self.voteup:
            if (self.votedown*100)/(self.voteup+self.votedown) >= 60:
                return True

        return False

@python_2_unicode_compatible
class Suggest_vote(models.Model):
    user = models.ForeignKey('Users', on_delete=models.CASCADE)
    suggest = models.ForeignKey('Suggest', on_delete=models.CASCADE)

    def __str__(self):
        return "User : "+self.user.user.username+" has voted for :"+str(self.suggest.id)


@python_2_unicode_compatible
class Trash(models.Model):
    trashContent = models.TextField()
    userDisplayName = models.CharField(max_length=40)
    datePublish = models.DateField()
    commentCount = models.SmallIntegerField(default=0)
    favoriteCount = models.SmallIntegerField(default=0)
    voteUp = models.SmallIntegerField(default=0)
    voteDown = models.SmallIntegerField(default=0)
    idUsers = models.ForeignKey('Users', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.trashContent
    @staticmethod
    def create(s):
        Trash.objects.create(trashContent=s.content, datePublish=s.datesubmit, idUsers=s.idUsers, userDisplayName=s.idUsers.user.username)


