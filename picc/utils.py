from datetime import datetime, timedelta

from django import template
from django.utils import html


import random
import string

register = template.Library()

@register.filter
def get_type(value):
    return type(value)

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'upload/{0}/{1}'.format(instance.user.id, filename)

def timesince(m_days=10):
    return datetime.now()-timedelta(days=m_days)

def escapeHTML(text):
    return html.conditional_escape(text)

def getRnd(min, max):
    return random.randint(int(min), int(max))

def textRnd(length=32):
    chars = string.ascii_letters+string.digits
    return ''.join(random.choice(chars) for c in range(length))

def rndsi(max):
    return str(random.randrange(0, max))

