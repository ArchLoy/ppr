from django import forms
from django.forms import ModelForm
from django.forms.utils import ErrorList
from . import models

from django.contrib.auth.models import User
from captcha.fields import CaptchaField

def setError(form, fieldname, error):
    if not fieldname in form._errors.keys():
        form._errors.setdefault(fieldname, ErrorList())

    form._errors[fieldname].append(error)

class UserForm(ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = User
        fields = ["username", "password", "email"]
        widgets = {
            'password': forms.PasswordInput(),
        }

class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = ["username", "password"]
        widgets = {
                    'password': forms.PasswordInput(),
                  }

class SuggestForm(ModelForm):
    class Meta:
        model = models.Suggest
        fields = ["content"]
        widgets = {
                     'content': forms.Textarea(attrs=
                             {
                                'data-provide':'markdown',
                                'data-hidden-buttons':'cmdImage'
                             }),
                  }

class ConfirmUserForm(forms.Form):
    username = forms.CharField(required=True,
                    widget=forms.TextInput(attrs={'class': "form-control"}))
    email = forms.EmailField(required=True,
                    widget=forms.TextInput(attrs={'class': "form-control"}))
    confirmCode = forms.CharField(required=True, min_length=32, max_length=32,
                    widget=forms.TextInput(attrs={'class': "form-control"}))
    captcha = CaptchaField()


class VoteForm(forms.Form):
    vote = forms.NullBooleanField(required=True)
    id = forms.IntegerField(required=True)

    def __init__(self, *args, **kwargs):
        super(VoteForm, self).__init__(*args, **kwargs)

        self.fields["vote"].widget = forms.widgets.HiddenInput()
        self.fields["id"].widget = forms.widgets.HiddenInput()
        #TODO : Set widget parameter on field directly


    @staticmethod
    def getSuggestToVote(user):

        suggest = None
        # Get all suggests and remove all not voted by user
        # Note that method is useful for minimal suggests in database
        alls = models.Suggest.objects.all()
        for s in alls:
            if not s.suggest_vote_set.filter(user_id=user.id).exists():
                suggest = s
                break

        return suggest


class CommentForm(ModelForm):
    class Meta:
        model = models.Comments
        fields = ['userDisplayName', "comm", "picc", "idComments"]
        widgets = {
                     'comm': forms.Textarea(attrs=
                             {
                                'data-provide':'markdown',
                                'data-hidden-buttons':'cmdImage'
                             }),
                     'picc': forms.HiddenInput(),
                     'idComments': forms.HiddenInput(),
                  }
        labels = {
                    'userDisplayName': 'Pseudo',
                    'comm': 'Exprimez-vous !',
                 }
        help_texts = {
                    'userDisplayName': 'Pseudo sous lequel appraîtra votre commentaire',
                }


