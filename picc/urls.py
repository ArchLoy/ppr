from django.conf.urls import url
from django.contrib.auth import views as auth_views


from . import views

urlpatterns = [
    url(r'^login$', views.authUser),
    url(r'^logout$', auth_views.logout, {'next_page': '/'} ),
    url(r'^change-password/', auth_views.password_change),
    url(r'^confirm$', views.confirmAccount),
    ]
