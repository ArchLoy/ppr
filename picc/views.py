from django.shortcuts import render
from django.shortcuts import redirect

from django.http import HttpResponse
from django.http import Http404
from django.http import JsonResponse

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required

from django.contrib.auth.forms import AuthenticationForm

from django.views.decorators.csrf import ensure_csrf_cookie

from .models import *
from . import piccForms
from .decorators  import group_required

from . import utils
from . import md_utils

import pprint

# ----------------------------------------------------------------------
#
# Utils functions
#
#



# ----------------------------------------------------------------------
#
# Views
#
#

def authUser(request):
    if request.method != 'POST':
        return Http404()

    lform = AuthenticationForm(data=request.POST)

    if lform.is_valid():
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                # Redirect to a success page.
                return HttpResponse('OK')
            else:
                # Return a 'disabled account' error message
                return  HttpResponse('disabled_account.html')
        else:
            # Return an 'invalid login' error message.
            raise Http404("invalid login")
    else:
        return loginForm(request, lform)

def loginForm(request, lform=None):
    m_status = 200
    error = False
    if not lform:
        lform = AuthenticationForm()
    else:
        m_status = 400
        error = True

    context = {'login_form':lform, 'error':error}

    if not error:
        return render(request, 'loginView.html.tpl', context, status=m_status)
    else:
        return render(request, 'loginForm.html.tpl', context, status=m_status)

def index(request, id=1):
    id = int(id)
    if(id == 0):
        raise Http404()

    piccs = Picc.objects.order_by('-id')[(id-1)*10:(id-1)*10+10]

    context = { 'piccs': piccs, 'page_u': id+1, 'page_d': id-1 }

    return render(request, "index.html.tpl", context)

def random(request):
    max = Picc.objects.count()/10
    pagenum = utils.getRnd(0, max)
    return index(request, pagenum)


def picc(request, pnum):

    pnum = int(pnum)

    m_picc = Picc.objects.get(id=pnum)
    m_comms = m_picc.comments_set.filter(idComments_id=None).order_by('datePublish')
    pprint.pprint(m_comms.count())
    context = { "picc" : m_picc, "comms":m_comms, 'comment_form': piccForms.CommentForm(initial={'userDisplayName': request.user.username}) }

    return render(request, "picc.html.tpl", context)

@login_required
@group_required('base_user')
def suggestNew(request):
    if request.method == 'POST':
        # Limit active suggest : 6 per user
        if (request.user.users.suggest_set.count() < 6):
            user = request.user.users

            sug = Suggest(idUsers=user)  #TODO : poor code
            # create suggest object by modelform
            sugF = piccForms.SuggestForm(request.POST, instance=sug)
            sugF.save()

            # send empty form to page
            m_suggest_form = piccForms.SuggestForm()
            context = { "suggest_form": m_suggest_form }
            return render(request, "suggest.html.tpl", context)
        else:
            return HttpResponse("too many suggest in act")
    else:
        m_suggest = piccForms.SuggestForm()
        context = { "suggest_form": m_suggest }
        return render(request, "suggest.html.tpl", context)

@login_required
@group_required('validator')
def suggestVote(request):
    # Try to get suggest to vote on
    suggest = piccForms.VoteForm.getSuggestToVote(request.user.users)

    if not suggest:
        # No new suggest to vote on, sorry
        # TODO : Draw nice view in place of that
        return HttpResponse("no suggest to you")
    else:
        voteform = piccForms.VoteForm(initial={'id':suggest.id})
        request.session['id_sugg_form']=suggest.id
        return render(request, "vote.html.tpl", { "voteForm" : voteform, "content" : suggest.content })

@login_required
@group_required('validator')
def suggestVoteDone(request):
    if request.method == "POST":
        respForm = piccForms.VoteForm(request.POST)
        in_sess_id = request.session.get('id_sugg_form', -1)
        del request.session['id_sugg_form']

        # If form is valid and session id too
        if respForm.is_valid() and in_sess_id==respForm.cleaned_data['id']:

            # Get suggest from id and add new vote
            m_suggest = Suggest.objects.get(pk=respForm.cleaned_data['id'])
            m_suggest.addVote(respForm.cleaned_data['vote'], request.user.users)

            # Try to get next suggest to vote on
            m_suggest_next = piccForms.VoteForm.getSuggestToVote(request.user.users)
            if m_suggest_next:
                # set next suggest id in session
                request.session['id_sugg_form']=m_suggest_next.id
                # create and send new form
                voteForm = piccForms.VoteForm(initial={'id':m_suggest_next.id})
                return render(request, "voteSuggestForm.html.tpl", { "voteForm" : voteForm, "content" : m_suggest_next.content })
            else:
                # No new suggests to vote on.
                # TODO : use same view with info text and disabled buttons
                return HttpResponse("No next")
        else:
            # Invalid id or form
            # TODO : create a error message
            # return HttpResponse("invalid")
            return render(request, "voteSuggestForm.html.tpl", {'voteForm':respForm, "content":''})
    # Try hack :)
    return HttpResponse("")

@login_required
@group_required('base_user')
def suggestSuccess(request):
    return "<h1>Hello to success submit</h1>"

def test(request):
    pprint.pprint(request.POST)

    return render(request, "test.html.tpl")

def validateNewUser(request):
    _form = piccForms.UserForm(request.POST)
    if _form.is_valid():
        username = request.POST["username"]
        password = request.POST["password"]
        email = request.POST["email"]

        # Do not using form.save() in order to use picc_user create_user function
        m_user = Users.create_user(username, password, email)

        if m_user != None:
            return render(request, "confirm.html.tpl")
        else:
            return JsonResponse({'error':'uncreated'})
    else:
        context = { "form" : _form }
        return render(request, "register.html.tpl", context, status=400)


def register(request):
    if request.method == 'POST':
        return validateNewUser(request)
    else:
        m_form = piccForms.UserForm()
        context = { 'form': m_form }
        return render(request, "register.html.tpl", context)
# Create your views here.


@login_required
@group_required('staff')
@ensure_csrf_cookie
def validatePiccs(request):
    if request.method == 'POST':
        POST = request.POST
        id = int(POST.get("id", -1))

        if id == -1:
            return HttpResponse("-_-")

        suggest = Suggest.objects.get(pk=id)
        if suggest.get_ratio() >= 66 or suggest.is_old():
            if suggest.is_voteup:
                Picc.create(suggest)
            elif not suggest.is_votedown:
                Trash.create(suggest)
            suggest.delete()
        return HttpResponse()
    else:

        # TODO: Set time delta to non arbitrary value
        # TODO: Set dynamic suggest_vote count (60%)
        suggests = (Suggest.objects.filter(datesubmit__lte=utils.timesince(15)) |
                   Suggest.objects.filter(suggest_vote__gte=Group.objects.filter(name='validator').count()))
        suggests = suggests.order_by('-datesubmit').distinct()
        return render(request, "validate-picc.html.tpl", {'suggests':suggests})


def parseMD(request):
    ret = md_utils.processMd(request.GET['data'])
    return HttpResponse(ret)

def confirmAccount(request):
    if request.method == 'POST':
        m_form = piccForms.ConfirmUserForm(request.POST)
        if m_form.is_valid():
            try:
                u = User.objects.get(username=m_form.cleaned_data['username'], email=m_form.cleaned_data['email'])
            except:
                piccForms.setError(m_form, 'username', 'Utilisateur ou email erroné')
                piccForms.setError(m_form, 'email', '')
                return render(request, "confirmAccount.html.tpl", {'form':m_form})
            piccu = u.users
            if piccu.confirmCode == m_form.cleaned_data['confirmCode']:
                u.is_active=True
                piccu.confirmCode = ''
                u.save()
                piccu.save()
                return render(request, "confirmAccount.html.tpl", {'successfull':True})
            else:
                piccForms.setError(m_form,'confirmCode','Code erroné')
                pprint.pprint(m_form._errors)
                return render(request, "confirmAccount.html.tpl", {'form':m_form})
        else:
            pprint.pprint(m_form._errors)
            return render(request, "confirmAccount.html.tpl", {'form':m_form})
    else:
        confirmForm = piccForms.ConfirmUserForm()
        return render(request, "confirmAccount.html.tpl", {'form':confirmForm})


def addComment(r):
    if r.method == 'POST':
        content = r.POST.get('content', '')
        uname = r.POST.get('userDisplayName', '')
        related = r.POST.get('relatedComm', -1)
        return HttpResponse('OK')
    raise 404


def voteForPicc(r):
    if r.method == 'POST':
        pid = r.POST.get('picc_id', -1)
        vote = r.POST.get('voteUp', -1)

        if pid != -1 != vote:
            if r.user.is_authenticated():
                if r.user.users.userVotes.filter(id=pid).exists():
                    return HttpResponse("")
                else:
                    r.user.users.userVotes.add(Picc.objects.get(id=pid))
            else:
                votedPiccs=r.session.get('voted_piccs', [])
                if pid in votedPiccs:
                    return HttpResponse("")
                else:
                    votedPiccs.append(int(pid))
                    r.session['voted_piccs']=votedPiccs

            picc = Picc.objects.get(id=pid)
            if vote == 0:
                picc.voteDown += 1
                r.user.users.piccVotesDown += 1
            else:
                picc.voteUp += 1
                r.user.users.piccVotesUp += 1

            picc.save()
            r.user.users.save()

            return HttpResponse("OK")

def sendComment(r):
    if r.method == 'POST':
        form = piccForms.CommentForm(data=r.POST)
        if form.is_valid():
            comm = form.instance
            comm.idUsers = r.user.users
            if not comm.userDisplayName:
                comm.userDisplayName = r.idUsers.user.username

            if comm.idComments and comm.idComments.idComments and comm.idComments.idComments.idComments:
                return HttpResponse("404");

            comm.save()

            third = False
            if comm.idComments and comm.idComments.idComments:
                third = True

            return render(r, "comment.html.tpl", {'comm':comm, 'lvl3':third})

    return HttpResponse("KO")
