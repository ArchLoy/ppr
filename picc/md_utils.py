from django.utils import html
import markdown
from pylibs.markdown import ppr_extensions

import re

QUOTE_RE='(.*?\n|^)(>)(.*?)(\n.*?)?$'

regex = re.compile(QUOTE_RE, flags=re.DOTALL)

def escapeMd(raw_text):
    prepared = raw_text.replace('```', '`').replace('`', '```')

    sub = prepared.split('```')
    ftext=''
    i=0
    for item in sub:
        if not i%2:
            item = html.conditional_escape(item)
        else:
            item = '```'+item+'```'
        
        ftext += item 
        i+=1

    return ftext


def prepareMd(raw_text):

    tt = ''

    m = regex.match(raw_text)
    if m:
        grp = m.groups()
        while(grp):
            tt+=escapeMd(grp[0])+grp[1]
            tt+=html.conditional_escape(grp[2])
            if grp[3]:
                m = regex.match(grp[3])
                if m:
                    grp = m.groups()
                else:
                    tt+=escapeMd(grp[3])
                    grp=None
            else:
                grp=None
    else: 
        tt = escapeMd(raw_text)
    return tt 


def processMd(raw_text):
    text = prepareMd(raw_text)
    exts = ['markdown.extensions.fenced_code', 'markdown.extensions.nl2br']
    exts.append(ppr_extensions.RemoveImageExtention())
    exts.append(ppr_extensions.TxtTagsExtension())
    return markdown.markdown(text, exts)
