from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Picc)
admin.site.register(models.Comments)
admin.site.register(models.Users)
admin.site.register(models.Suggest)
