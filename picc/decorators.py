from django.http import HttpResponseForbidden
from django.core.exceptions import PermissionDenied

def group_required(groupName):
    def decorator(view):
        def wrapper(request, *args, **kwargs):
            user = request.user

            if groupName in user.groups.values_list('name', flat=True):
                return view(request, *args, **kwargs)
            else:
                # return HttpResponseForbidden("for : Bidden only") # 403 Forbidden is better than 404
                raise PermissionDenied
        return wrapper
    return decorator
