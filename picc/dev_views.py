from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

from .decorators  import group_required

from django.http import JsonResponse

from subprocess import check_output

@login_required
@user_passes_test(lambda u: u.is_superuser)
def pull(request):
    out = check_output(['git', 'pull'])
    return JsonResponse({'output':str(out)})

