from django.core.mail import send_mail

CONFIRM_ROUTE='http://picc.archloy.xyz/'+'users/confirm'

def send_confirmation_mail(user):
    subject = 'Picc user confirmation'
    body = "Bienvenue "+user.user.username+\
           "\n Pour confirmer et activer votre compte en picc*,"+\
           "veuillez vous rentre à l'adresse :"+CONFIRM_ROUTE+\
           " et recopier le code suivant : \n"+user.confirmCode+"\n"+\
           "À très bientôt !\n\n\n"+"*: on dit bien un compte en banque..."

    send_mail(subject, body, 'no-reply@archloy.xyz', [user.user.email, user.user.email], fail_silently=False)
#    send_mail(subject, body, 'no-reply@archloy.xyz', [user.user.email], fail_silently=False)
