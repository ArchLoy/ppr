{#   index.html.tpl #}

{% extends 'base.html.tpl' %}
{% load staticfiles %}

{% block contents %}
	<link rel="stylesheet" href="{% static 'css/validate-picc.css' %}" type="text/css">

    {% for suggest in suggests %}
	<div id="panel_{{ suggest.id }}" class="panel panel-primary">
		<div class="panel-heading panel-title">
		En attente depuis : {{ suggest.datesubmit|timesince }}
		</div>
		<div class="panel-body picc-content">
			<div>
				<span class="label label-primary"> {{ suggest.voteup }}+ </span><br/><br/>
				<span class="label label-danger">  {{ suggest.votedown }}- </span>
			</div>
			<div><span class="badge" data-toggle="tooltip"  title="Texte explicatif">{{ suggest.get_ratio }}%</span></div>
			<div>
				{% autoescape off %}	
				{{ suggest.content }}			
				{% endautoescape %}
			</div>
		</div>
		<div class="panel-footer">
			{% if suggest.is_voteup %}
				<button class="btn btn-success" id="button_{{ suggest.id }}" onclick="validAction({{ suggest.id }})">Je valide ce Picc</button>
			{% elif suggest.is_votedown %}
				<button class="btn btn-danger" id="button_{{ suggest.id }}" onclick="validAction( {{ suggest.id }} )">Détruire définitivement</button>
			{% else %}
				<button class="btn" id="button_{{ suggest.id }}" onclick="validAction( {{ suggest.id }} )">Vox populi, Vox dei</button>
			{% endif %}

			<button class="btn" id="wait" onclick="delSuggest({{ suggest.id }})">Wait and See</button>
		</div>
	</div>
	{% endfor %}

	<script type="text/javascript" >

	function delSuggest(id)
	{
		$('#panel_'+id).remove()
	}

	function validAction(id)
	{
		$("#button_"+id).addClass('disabled');
		$.post("{% url 'validatePicc' %}", { 'id':id }).done(delSuggest(id));
	}

	function getToken()
	{
		return $.cookie("picc_csrf_token")
	}

	function prepareAjax()
	{
		$.ajaxSetup(
		{
			beforeSend: function(xhr, settings) 
			{
				xhr.setRequestHeader("X-CSRFToken", getToken());
			}
		});
	}

	$.ready(prepareAjax());
</script>

<script src={% static 'jquery/jquery.cookie.js' %}></script>
{% endblock %}
