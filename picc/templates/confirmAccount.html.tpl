{#   confirmAccount.html.tpl #}
    
{# User confirmation page #}

{# Inherits base twig template, whitch include bootstrap, jquery, etc #}
{% extends 'base.html.tpl' %}

{# Overwrite body from base.twig #}
{% block contents %}
    {% if successfull %} 
        <div class="jumbotron text-center">
            <p>Compte confirmé avec succès. Vous pouvez dès à présent vous authentifier</p>
        </div>
    {% else %}
        <form id="regForm" class="form-horizontal col-md-offset-3" action="/users/confirm" method="post">
            <div class="form-group">
                {% for err in form.non_field_errors %}
                    <p class="text-danger">{{ err }}</p>
                {% endfor %}

                {% csrf_token %}
                
                {% for field in form %}
                    <div class="form-group {%if field.errors %}has-error{%endif%}">
                            <label class="col-md-2 control-label" for="{{ field.auto_id }}">{{ field.label }}</label>
                        
                            <div class="col-md-4"> 
                                {{ field }}

                                {% if field.errors %}
                                    <p class='text-danger'>{{ field.errors.as_text }}</p>
                                {% endif %}
                            </div>
                    </div>
                {% endfor %}
                <button type='submit' class="btn btn-success col-md-offset-2">Envoyer</button>
            </div>
        </form>
    {% endif %}
{% endblock %}
