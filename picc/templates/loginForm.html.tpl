	<form id="loginForm" class="form-horizontal" action="/users/login" method="post">

		<div class="form-group">
        {% for err in login_form.non_field_errors %}
            <p class="text-danger">{{ err }}</p>
        {% endfor %}
            {% csrf_token %}
            {% for field in login_form %}
                <div class="form-group {%if field.errors %}has-error{%endif%}">
                    <p>{{ field.label_tag }}</p><p> {{ field }}</p>
                </div>
            {% endfor %}
		</div>
	</form>
