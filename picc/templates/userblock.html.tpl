<div class='panel-heading'>
        Welcome <strong>{{ request.user.get_username }}</strong>
</div>
<div class='panel-body'>
    <img class="img-responsive img-thumbnail pull-left" style="margin-right: 0.5em;" src="{{ request.user.users.avatar }}" alt="" width="96" height="96"></img>
    {% with request.user.users as puser %}
    <p><strong>{{ puser.picc_set.count }}</strong> Picc à mon actif <br />
    <span class="tabtext">Dont {{ puser.piccVotesUp }} votes positifs !</span><br />
    <strong>{{ puser.comments_set.count }}</strong> commentaires postés</p>
    <a href="profile/">Edit Profile </a><br />
    <a href="/users/logout">Log out</a><br />
    {% endwith %}
</div>
