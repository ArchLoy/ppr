    <div id='{{ comm.id }}' class="panel-heading">
        <img class="img-responsive img-thumbnail pull-left" style="margin-right: 0.5em;" src="{{ comm.idUsers.avatar }}" alt="{{ comm.userDisplayName }}" width="32" height="32"></img>
        <p><strong>{{ comm.userDisplayName }}</strong></p>
    </div>
    <div class="panel-body">
		{% autoescape off %}	
            <p>{{ comm.comm }}</p>
		{% endautoescape %}
    </div>
    <div class="panel-footer">
        <a href=".#{{ comm.id }}"><span class="label label-default"><u>#{{ comm.id }}</u></span></a>
        <span class="label label-warning">{{ comm.datePublish|date:'d/m/Y H:i' }}</span>
        {% if not lvl3 %}
            <button class='btn btn-default btn-xs pull-right' onclick='deployCommentForm("#comm_{{ comm.id }}_panel")'>Commenter</button>
        {% endif %}
    </div>
