{#    piccBundle::picc.html.twig    #}
    
{#    View specified Picc     #}

{% load staticfiles %}

	<div id="picc_{{ picc.id }}" class="panel panel-default picc">
        <div class="panel-body">
				{% autoescape off %}	
    			{{ picc.piccContent }}	
				{% endautoescape %}
        </div>
        {# Picc informations in footer #}
        <div id="picc_footer" class="panel-footer">
            {% if linkit %}
                <a href="{% url 'picc_view' picc.id %}"><span class="label label-default"><u>#{{ picc.id }}</u></span></a> 
            {% else %}
                <span id="picc_id" class="label label-default">#{{ picc.id }}</span>
            {% endif %}
            <span class="label label-default">{{ picc.datePublish|date:'d/m/Y' }}</span>
            <span class="label label-default">{{ picc.userDisplayName }}</span>
            <span id="valvoteup" class="label progress-bar-success">+ {{ picc.voteUp }}</span>
            <span id="valvotedown" class="label progress-bar-danger">- {{ picc.voteDown }}</span>
            <span class="label label-comms">comms {{ picc.comments_set.count }}</span>
            <div id="pvdiv" class="piccvote">
                {% if not picc.id in request.session.voted_piccs and not picc in request.user.users.userVotes.all %}
                    <span id="btnVoteUp" class="label label-success btn-vote" onclick="piccvote({{ picc.id }}, 1)"><ins>C’est vrai que ça Picc !!</ins></span>
                    <span id="btnVoteDown" class="label label-danger btn-vote" onclick="piccvote({{ picc.id }}, 0)"><ins>C’est toi le cactus...</ins></span>
                {% else %}
                    <span class="label">Mieux vaut ne pas s'assoir ici...</span>
                {% endif %}
            </div>
        </div>
    </div>
