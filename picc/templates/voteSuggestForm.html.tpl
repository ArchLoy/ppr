<div id="suggest-content" class="panel-body">
{% autoescape off %}	
{{ content }}
{% endautoescape %}
</div>
<div class="panel-footer">
<form id="voteForm">
    {% csrf_token %}
	{{ voteForm }}
    <li><button type="button" class="btn btn-success" id="voteUp" onclick="voteClick(this)" >Mérite d'exister</button></li>
	<li><button type="button" class="btn btn-danger" id="voteDown" onclick="voteClick(this)">Le tartare n'en voudrait pas</button></li>
</form>
</div>
