{# Inherits base twig template, whitch include bootstrap, jquery, etc #}
{% extends 'base.html.tpl' %}

{% load staticfiles %}

{# Overwrite css block #}
{% block stylesheets %}
	<link rel="stylesheet" type="text/css" media="screen" href="{% static 'bs-md/bootstrap-markdown.min.css' %}">
{% endblock %}
{# Overwrite body from base.html.tpl#}
{% block contents %}

	<form action="/suggest/new" method="post">
		<div class="form-group">
		{% csrf_token %}
		{{ suggest_form.as_p}}
		<input type="submit" value="Submit" />
		</div>
	</form>

{% endblock %}

{% block javascripts %}
	<script src="{% static 'js-md/markdown.js' %}"></script>
	<script src="{% static 'js-md/to-markdown.js' %}"></script>
	<script src="{% static 'js/bootstrap-markdown.js' %}"></script>
	<script src="{% static 'bs-md/bootstrap-markdown.fr.js' %}"></script>
{% endblock %}
