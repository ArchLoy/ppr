{#   index.html.tpl #}
    
{#   Index page, will show the 10's last Picc #}

{# Inherits base twig template, whitch include bootstrap, jquery, etc #}
{% extends 'base.html.tpl' %}

{% load staticfiles %}

{% block stylesheets %}

    <link href="{% static 'css/picc.css' %}" type="text/css" rel="stylesheet"/>

{% endblock %}

{# Overwrite body from base template #}
{% block contents %}

  {% if piccs %}
    {# For each picc in piccs, draw it as bootstrap::panel #}
    {% for picc in piccs %}
        {% include 'picc_panel.html.tpl' with picc=picc linkit=True %}
    {% endfor %}
	<a href="{{ page_u }}"><button class="btn" >Page suivante</button></a> 
	{% if page_d > 0 %} 
		<a href="{{ page_d }}"><button class="btn">Page Précédente</button></a>
	{% endif %}
   
  {% else %}
	<p>Bonjour. Cette page n'existe pas encore...</p>
	<p>Certes, une étude statistique pourrait donner, approximativement, la date à laquelle cette page existera.</p>
    <p>Néanmoins, la propension de systèmes autonomes, l'évolution des connaissances et de l'éducation informatique peut retarder considérablement l'apparition de cette page.<br />
    Au delà de cela, la bêtise humaine étant ce qu'elle est, on a bon espoir de voir cette page arriver un jour...<br />
    En attendant, il est inutile de taper un nombre au pif dans la barre d'adresse ;)</p>
  {% endif %}
{% endblock %}

{% block javascripts %}
    <script src="{% static 'jquery/jquery.cookie.js' %}"></script>
    <script src="{% static 'js/prepare.js'%}"></script>
	<script src="{% static 'js/picc_panel.js' %}"></script>
{% endblock %}
