
<nav class="navbar navbar-default navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="/">PPR</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="/">Home</a></li>
			<li><a href="/random">Random</a></li>

			{% if request.user.is_authenticated %}
				<li><a href="/suggest/new">Soumettre</a></li>
				{% if perms.suggests.can_add_suggest_vote %}
					<li><a href="/suggest/vote">Voter</a></li>
				{% endif %}
				{% if perms.suggests.can_add_picc %}
					<li><a href="/suggest/validate">Valider</a></li>
				{% endif %}
				{% if request.user.is_superuser %}
					<button class="btn navbar-btn" onclick="gopull()">Dev : Pull</button>
				{% endif %}
			{% endif %}
		</ul>
	</div>
</nav>
{% if request.user.is_superuser %}
<div class="collapse" id="collapse">
	<div id="collapseWell" class="well" onclick="$('#collapse').collapse('hide');">

	</div>
</div>
<script type="text/javascript">
	function gopull()
	{
		$.get('/git/pull').done(function(data)
					{
						$('#collapse').collapse()
						$('#collapseWell').append("<p>"+data.output+"</p>");
					});
	}
</script>
{% endif %}

