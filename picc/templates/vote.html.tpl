{# Base validation template #}
{# Inherits base twig template, whitch include bootstrap, jquery, etc #}
{% extends 'base.html.tpl' %}

{% load staticfiles %}

{% block stylesheets %}
	<link rel="stylesheet" href="{% static 'hljs/default.min.css'%}">
{% endblock %}

{# Overwrite body from base.twig #}
{% block contents %}

	<div id="voteFormContent" class="panel panel-default text-left">
			{% include "voteSuggestForm.html.tpl" %} 
	</div>

<script type="text/javascript" >
	function voteClick(btn)
	{
		var m_form = document.getElementById("voteForm");
		m_form.elements.namedItem("vote").value = btn.id=="voteUp";
		
		$.post("vote/done", $("#voteForm").serialize()).done(function(datas)
				{
					$('#voteFormContent').html(datas);
				});
		
	}
</script>

{% endblock %}

{% block javascripts %}
{% endblock %}
