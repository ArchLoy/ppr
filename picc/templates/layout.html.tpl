{% load staticfiles %}

<!-- app/Resources/views/base.html.twig -->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
        <title>{% block title %}UFE:IBTR{% endblock %} : User Fatal Error : Insert Brain Then Restart </title>
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		<link href="{% static 'css/fonts.css' %}" type="text/css" rel="stylesheet"/>
		<link rel="stylesheet" href="{% static 'bootstrap-themes/bootstrap/bootstrap.min.css' %}" type="text/css">
		<link rel="stylesheet" href="{% static 'css/global.css' %}" type="text/css">
		<link rel="stylesheet" href="{% static 'css/font-awesome.min.css' %}" type="text/css">
        {% block stylesheets %}
        {% endblock %}
		<script type="text/javascript" src={% static 'jquery/jquery-2.2.4.js' %}></script>

	   
{# <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" /> #}
    </head>
	<body class="col-md-12">
        <section id="wrapper" class="col-md-12">

            <header class="page-header">
                <div class="page-header">
                    <h1>UFE:IBTR <small>User Fatal Error : Insert Brain Then Restart</small></h1>
                </div>
                {% block header %}
                {% endblock %}
            </header>
  
			{% block body %}{% endblock %}

           <aside class="sidebar">
               {% block sidebar %}{% endblock %}
           </aside>

           <div id="footer">
               {% block footer %}
               {% endblock %}
           </div>
       </section>
    </body>

	<script src="{% static 'bootstrap/js/bootstrap.min.js' %}" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<script src="{% static 'hljs/highlight.min.js'%}"></script>
	<script>hljs.initHighlightingOnLoad();</script>
    {% block javascripts %}
	{% endblock %}

</html>
