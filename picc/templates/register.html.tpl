
	<div id="modal_content">
		<div class="modal-header"><h4 class="modal-title" id="myModalLabel">Register new account</h4></div>
		<div class="modal-body">
			{% include "register_content.html.tpl" %}
		    <button id="register-btn" type="submit" class="btn btn-default modal-submit" >S'enregistrer</button>
		</div>
	</div>
