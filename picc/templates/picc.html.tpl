{#    piccBundle::picc.html.twig    #}
    
{#    View specified Picc and comments    #}

{# Inherits base html template, whitch include bootstrap, jquery, etc #}
{% extends 'base.html.tpl' %}

{% load staticfiles %}

{% block stylesheets %}
	<link href="{% static 'bs-md/bootstrap-markdown.min.css' %}" rel="stylesheet" type="text/css" media="screen">
    <link href="{% static 'css/picc.css' %}" type="text/css" rel="stylesheet"/>
    <link href="{% static 'css/picc_comms.css' %}" type="text/css" rel="stylesheet"/>
{% endblock %}

{# Overwrite body from base.html.tpl #}
{% block contents %}

    {% include 'picc_panel.html.tpl' with picc=picc linkit=False %}
    <div class="panel panel-default panel-comms">
        <div class="panel-heading">
            Commentaires avisés
            <button class='btn btn-default btn-xs pull-right' onclick='deployCommentForm("#panel_avise")'>Commenter</button>
        </div>
        <div id="panel_avise" class="panel-body">
        {% for comm in comms %} 
            <div id="comm_{{ comm.id }}_panel" class="panel panel-default">
                {% include 'commentBlock.html.tpl' with comm=comm %}
                {% if comm.comments.count != 0 %}
                    <ul class="list-group">
                        {% for comm2 in comm.comments.all|dictsort:"datePublish" %}
                            <li class="list-group-item">
                                <div id="comm_{{ comm2.id }}_panel" class="panel panel-default">
                                    {% include 'commentBlock.html.tpl' with comm=comm2 %}
                                    {% if comm2.comments.count != 0 %}
                                        <ul class="list-group">
                                            {% for comm3 in comm2.comments.all|dictsort:"datePublish" %}
                                                <li class="list-group-item">
                                                    <div id="comm_{{ comm3.id }}_panel" class="panel panel-default">
                                                        {% include 'commentBlock.html.tpl' with comm=comm3 lvl3=True %}
                                                    </div>
                                                </li>
                                                {% if forloop.last %}
                                                    <span id="commentBtnBottom" class="text-right">
                                                    <button class='btn btn-default btn-xs' onclick='deployCommentForm("#comm_{{ comm2.id }}_panel")'>Commenter</button>
                                                    </span>
                                                {% endif %}
                                            {% endfor %}
                                        </ul>
                                    {% endif %}
                                </div>
                            </li>
                            {% if forloop.last %}
                                <span id="commentBtnBottom" class="text-right">
                                <button class='btn btn-default btn-xs' onclick='deployCommentForm("#comm_{{ comm.id }}_panel")'>Commenter</button>
                                </span>
                            {% endif %}
                        {% endfor %}
                    </ul>
                {% endif %}
            </div>
            {% if forloop.last %}
                <span id="commentBtnBottom" class="text-right">
                <button class='btn btn-default btn-xs' onclick='deployCommentForm("#panel_avise")'>Commenter</button>
                </span>
            {% endif %}
        {% endfor %}
        </div>
    </div>
    <div id="commCollapse" class="collapse">
        <div id="commWell" class="well">
            <form id="commentForm">
                <div class="form-group">
                    {% csrf_token %}
		            {{ comment_form.as_p}}
                    <button type="button" class='btn' onclick="sendComment($('#commCollapse').parent('div')[0].id)">Submit</button>
                </div>
            </form>
        </div>
    </div>



{% endblock %}

{% block javascripts %}

    <script src="{% static 'jquery/jquery.cookie.js' %}"></script>
	<script src="{% static 'js-md/markdown.js' %}"></script>
	<script src="{% static 'js-md/to-markdown.js' %}"></script>
	<script src="{% static 'js/bootstrap-markdown.js' %}"></script>
	<script src="{% static 'bs-md/bootstrap-markdown.fr.js' %}"></script>
    <script src="{% static 'js/prepare.js'%}"></script>
    <script src="{% static 'js/picc_panel.js'%}"></script>
	<script src="{% static 'js/picc_view.js' %}"></script>

{% endblock %}
