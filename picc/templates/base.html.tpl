{% extends 'layout.html.tpl' %}
{% block header %}
	{% include 'navbar.html.tpl' %}
{% endblock %}
{% block body %}


	<section id='2' class="col-md-9">
		{% block contents %}{% endblock %}
	</section>
	
	<section id='3' class="col-md-3">
	
		<div class="panel panel-default">
		{% if request.user.is_authenticated %}
			{% include 'userblock.html.tpl' %}
		{% else %}
            <div class="text-center">
                <br/>
                <p><button id="login_btn" class="btn btn-default" data-toggle="modal" data-target="#modal_view">S'authentifier</button>
                <button id="register_btn" class="btn btn-primary" data-toggle="modal" data-target="#modal_view">S'enregistrer</button></p> 
                <br/>
            </div>
		{% endif %}
		</div>

	</section>


    {% if not request.user.is_authenticated %}

        <div id="modal_view" class="modal fade" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-sm">
        <div class="modal-content" id="modal_class">
        </div>
        </div>
        </div>

        </p>
                
        <script src='/static/js/authentication.js'></script>
    {% endif %}
{% endblock %}
