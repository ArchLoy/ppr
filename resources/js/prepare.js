
	function getToken()
	{
		return $.cookie("picc_csrf_token")
	}

	function prepareAjax()
	{
		$.ajaxSetup(
		{
			beforeSend: function(xhr, settings) 
			{
				xhr.setRequestHeader("X-CSRFToken", getToken());
			}
		});
	}

	$(document).ready(function()
    {
        prepareAjax();
    });

