function sendComment(id)
{
    var m_form = $('#commentForm');
    var m_id;
    if(m_id == 'panel_avise')
    {
        m_id = null;
    }
    else
    {
        m_id = id.replace(/[^0-9]/g, '');
    }
    m_form[0].elements.namedItem("id_idComments").value = m_id;
    m_form[0].elements.namedItem("id_picc").value = $('#picc_id').text().replace('#', '');

    $.post("/sendcomment", m_form.serialize()).done(function(data)
    {
        var mid = "#"+id

        $("#commCollapse").collapse('toggle');

        if(id === "panel_avise")
        {
            $(mid).append($(data));
        }
        else if($(mid+' > ul > li:last').length != 0)
        {
            $('<li class="list-group-item">'+data+'</li>').insertAfter($(mid+' > ul > li:last'));
        }
        else
        {
            $(mid).append($('<ul class="list-group"><li class="list-group-item">'+data+'</li></ul>'));
        }
    });
}

docollapse = function(){};
var colpsrefcom;

    function deployCommentForm(refcid)
    {
        var refcom;
        var colps = $("#commCollapse");

        if (refcid === '-')
        {
            refcom = $('#panel_avise');
        }
        else 
        {
            refcom = $(refcid);
        }

        if(refcom.children('#commCollapse').length)
        {
            docollapse = function(){};
            colps.collapse('toggle');
        }
        else if(!colps.hasClass('collapse in'))
        {
            docollapse = function(){};
            refcom.append(colps);
            colps.collapse('toggle');
        }
        else
        {
            docollapse = function()
            {
                refcom.append(colps);
                colps.collapse('show');
            };

            colps.collapse('hide');
        }
    }

$(document).ready(function()
{
    $.fn.collapse.Constructor.TRANSITION_DURATION = 600;
    $('#commCollapse').on('shown.bs.collapse', function()
    {
        colp = $('#commCollapse');
        var npos;
        
        if(colp.height() <= $(window).height()*1.5)
        {
            npos=colp.offset().top-(( ($(window).height())*0.3 ))
        }
        else
        {
            npos = colp.offset().top;
        }

        $('html, body').animate({ scrollTop:npos }, 'slow');
    });

    $('#commCollapse').on('hidden.bs.collapse', function()
    {
        docollapse();
    });
});

