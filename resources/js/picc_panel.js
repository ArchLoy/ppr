
function piccvote(id, vote)
{
    $.post("/picc/vote", {'picc_id':id, 'voteUp':vote}, function(datas)
    {
        if(datas==="OK")
        {
            var valspan = vote ? valspan = $('#valvoteup') : valspan = $('#valvotedown');
            valspan.text(valspan.text().replace(/([+|-])\s(\d*)/, (str, signe, number) => `${signe} ${++number}`));
        }
        $("#picc_"+id+" #pvdiv").fadeOut('slow', function()
        {
            $("#picc_"+id+" #pvdiv").replaceWith(function()
            {
                return "<span id='pvdiv' class=\"label\" style='display:none'>" + "Merci, ta délation ne sera délaissée" + "</span>"; 
            });
            $("#picc_"+id+" #pvdiv").fadeIn('slow');
        });
        
    });
}

