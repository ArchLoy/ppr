
function prepareForms(type)
{
    if(type === 'login')
    $('#login-btn').on('click', function(e) 
    {
            var formAction = $('#loginForm').attr("action");
            $.post(formAction, $('#loginForm').serialize()).done(function(data)
            {
                location.reload(true);
            }).fail(function(data)
            {
                $('#loginForm').replaceWith(data.responseText);
            });
            return true;
    });

    else
    $('#register-btn').on('click', function(e) 
    {
        var formAction = $('#regForm').attr("action");
        $.post(formAction, $('#regForm').serialize()).done(function(data)
        {
            $('#modal_content').replaceWith(data);
            setTimeout(function() {$('#modal_view').modal('hide');}, 3000);
        }).fail(function(data)
        {
            $("#modal_content").replaceWith(data.responseText);
        });
        return true;
    });
}

function setModalDatas(type)
{
    act = 'register'
    if(type == 'login_btn')
        act='login'

    $.get('/'+act, function(data, status)
    {
        $('#modal_class').html(data);
        prepareForms(act);
    });
}

$(document).ready(function(){
        $("#modal_view").on('show.bs.modal', function (event) {
            btn_id=$(event.relatedTarget).prop('id')
            setModalDatas(btn_id)
    });
});
